#!/bin/bash
echo "File untuk di dekripsi?"
read filename
echo "Jam file dibuat?"
read hour
dekripsi(){
	A_ascii=65
	a_ascii=97
	alph=26
	while read -r line; do
		for ((i=0;i<${#line};i++)); do
			char=${line:$i:1}
                        if [[ "$char" =~ [a-z] ]]; then
                                ascii=$(printf "%d" "'$char'")
                                new_ascii=$((ascii - hour))
                                if [ $new_ascii -lt $a_ascii ]; then
                                        new_ascii=$((new_ascii + alph))
                                fi
				new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
                                echo -n "$new_char"
                        elif [[ "$char" =~ [A-Z] ]]; then
                                ascii=$(printf "%d" "'$char'")
                                new_ascii=$((ascii - hour))
                                if [ $new_ascii -lt $A_ascii ]; then
                                        new_ascii=$((new_ascii + alph))
                                fi
				new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
                                echo -n "$new_char"
                        else 
                                echo -n "$char"
                        fi      
                done
                echo ""
        done
}

tail "$filename" | dekripsi 
