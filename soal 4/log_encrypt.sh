#!/bin/bash

enkripsi(){
	Z_ascii=90 
	hour=$(date +"%H")
	if [ ${hour:0:1} == 0 ]; then
		hour=${hour:1:1}
	fi
	z_ascii=122
	alph=26
	while read -r line; do
		for ((i=0;i<${#line};i++)); do
			char=${line:$i:1}
			if [[ "$char" =~ [a-z] ]]; then
				ascii=$(printf "%d" "'$char'")
				new_ascii=$((ascii + hour))
				if [ $new_ascii -gt $z_ascii ]; then
					new_ascii=$((new_ascii - alph))
				fi
				new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
				echo -n "$new_char"
			elif [[ "$char" =~ [A-Z] ]]; then
				ascii=$(printf "%d" "'$char'")
                                new_ascii=$((ascii + hour))
                                if [ $new_ascii -gt $Z_ascii ]; then
                                        new_ascii=$((new_ascii - alph))
                                fi
                                new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
				echo -n "$new_char"
			else 
				echo -n "$char"
			fi	
		done
		echo ""
	done
}
name="$(date +'%H:%M %d:%m:%Y').txt"
cat /var/log/syslog | enkripsi > "$name"

#cron
# 0 */2 * * * log_encrypt.sh
