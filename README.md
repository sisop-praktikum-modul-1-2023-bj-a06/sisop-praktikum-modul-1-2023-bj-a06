# soal-shift-sisop-modul-1-ITSA06-2023
## Laporan Pengerjaan Soal Shift Modul 1 Praktikum Sistem Operasi

### Daftar Anggota :
1. Lihardo Marson Purba - 5025211238
2. Farhan Dwi Putra     - 5025211093
3. Victor Gustinova     - 5025211159

### Soal 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
- Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5   Universitas dengan ranking tertinggi di Jepang.
<br>

``` sh
#!/bin/bash

echo -e  "SOAL-1\n"
sort -t"," -n -k1 '2023 QS World University Rankings.csv' | awk -F "," '/Japan/ {print $1"    " $2"    "$3}' | head -n 5 
```
<br>

<br>

- [ ]    sort -t"," -n -k1 '2023 QS World University Rankings.csv' 
    <br>
        Merupakan command untuk melakukan sorting dari file csv 

- [ ]    awk -F "," '/Japan/ {print $1"    " $2"    "$3}' 
    <br>
        command untuk mengambil data dengan kata kunci Japan dari data yang sudah di-*sort*

- [ ]   head -n 5  
        Command untuk mengambil data 5 teratas dari yang sudah di-*sort*

<br>

- Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 

<br>

``` sh
#!/bin/bash

echo -e  "SOAL-2\n"
sort -t, -g -k9 '2023 QS World University Rankings.csv' | awk -F "," '/Japan/ {print $1"    " $2"    "$3"    FSR-Score ->"$9}' | head -5 
```
<br>

<br>

- [ ]    sort -t, -g -k9 '2023 QS World University Rankings.csv' 
    <br>
        Merupakan command untuk melakukan sorting dari file csv dengan ketentuan dari kolom 9

- [ ]    awk -F "," '/Japan/ {print $1"    " $2"    "$3}' 
    <br>
        command untuk mengambil data dengan kata kunci Japan dari data yang sudah di-*sort*

- [ ]   head -n 5  
        Command untuk mengambil data 5 teratas dari yang sudah di-*sort*

<br>

- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

<br>

``` sh
#!/bin/bash

echo -e  "SOAL-3\n"
sort -t, -n -k20 '2023 QS World University Rankings.csv' | awk -F "," '/Japan/ {print $1"    " $2"    "$3"    Ger-Rank->"$20}' | head -10
```
<br>

<br>

- [ ]    sort -t, -n -k20 '2023 QS World University Rankings.csv' 
    <br>
        Merupakan command untuk melakukan sorting dari file csv dengan ketentuan dari kolom 20

- [ ]    awk -F "," '/Japan/ {print $1"    " $2"    "$3}' 
    <br>
        command untuk mengambil data dengan kata kunci Japan dari data yang sudah di-*sort*

- [ ]   head -n 10  
        Command untuk mengambil data 10 teratas dari yang sudah di-*sort*

<br>

- Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

<br>

``` sh
#!/bin/bash

echo -e  "SOAL-4\n"
grep -i "keren" '2023 QS World University Rankings.csv'| awk -F "," '{print $1"    "$2"    "$3}'
```
<br>

<br>

- [ ]    grep -i "keren" '2023 QS World University Rankings.csv' 
    <br>
        Merupakan command line untuk mencari kata kunci "keren" dari kemungkinan berada di setiap colomn.

<br>

<br>

> Output nomor 1

![Screenshot_2023-03-10_105437](/uploads/175c66f2b3068350b125b79c060a11c0/Screenshot_2023-03-10_105437.png)

<br>

#### Kendala dan Catatan Soal 1
Kendalanya terdapat dalam urutan perintah yang hendak di implementasikan, dimana kita harus memperhatikan setiap parameter yang menjadi acuan dan fungsi standard output yang akan digunakan.

### Soal 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 

-  Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
    -    File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download        (perjalanan_1, perjalanan_2, dst)
    -    File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
<br>

```sh
hour=$(date +"%H")
if [ ${hour:0:1} == 0 ];then
	hour=${hour:1:1}
fi
if [ $hour == 0 ]; then
	hour=1
fi

mkfolder(){
	read urutan
	if [ -n $urutan ];then
		urutan=$(($urutan + 1))
		mkdir "kumpulan_$urutan"
		echo "kumpulan_$urutan"
	else 
		mkdir "kumpulan_1"
		echo "kumpulan_1"
	fi
}

download(){
        read folder
		cd "$folder"
        for ((count=2;count<=hour+1;count++)); do
		link=$(wget --user-agent 'Mozilla/5.0' -qO - "www.google.be/search?q=indonesia\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n1 | sed 's/.*src="\([^"]*\)".*/\1/')
                wget -O "perjalanan_$(($count - 2)).jpg" $link
        done
}
#download masuk folder
# 0 */10 * * *
ls | grep 'kumpulan_' | awk -F '_' '{print $2}' | awk -F '.' '{print $1}' | tail -n 1 | mkfolder | download

```
<br>

- [ ] Untuk mengambil nilai jam maka digunakan date+"%H". Karena ada kemungkinan nilai jam diawali dengan 0 maka nilai 0 tersebut harus dihapus. Perintah ${hour:0:1} akan mengambil karakter pertama pada hour. Jika karakter tersebut bernilai 0 maka akan hanya diambil karakter kedua saja dengan ${hour:1:1}

- [ ] Fungsi mkfolder akan mengambil nilai hasil pencarian urutan terakhir dan membuat folder urutan selanjutnya. Nama folder yang dibuat juga akan dioutputkan untuk dipakai pada fungsi selanjutnya.

- [ ] Fungsi download akan mendownload gambar kedalam folder hasil output fungsi mkfolder. Dalam fungsi ini akan dilakukan change directory menggunakan cd ke folder yang akan menjadi tempat gambar. Setelah itu dilakukan download sebanyak hour yang sudah didapatkan. Var link akan mengambil url gambar dengan menggunakan wget dan mencari bagian html dari hasil pencarian google tentang "Indonesia". Url yang didapatkan akan digunakan dalam wget untuk mendownload gambarnya.

- [ ] Untuk mengetahui urutan folder terakhir maka dilakukan ls. Dicari file atau folder bernama "kumpulan_". Ambil nilai angka yang ada setelah "_". Terakhir akan diambil nilai angka terbesar. Setelah itu maka dapat dijalankan fungsi mkdir dan download.

- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

```sh
mkzip(){
        read urutan
        if [ -n $urutan ];then
                urutan=$(($urutan + 1))
                echo "devil_$urutan.zip"
        else 
                echo "devil_1.zip"
        fi 
}


zipname=$(ls | grep 'devil_' | awk -F '_' '{print $2}' | awk -F '.' '{print $1}' | tail -n 1 | mkzip)

folds=$(ls | grep 'kumpulan_' | sort | tail -n 3) 

#melakukan zip
# 0 0 * * *
zip -r "$zipname" $folds

rm -r $folds
```
- [ ] Fungsi mkzip akan mengoutputkan nama zip yang harus dibuat.
- [ ] Nama zip akan disimpan pada zipname. Pada zipname akan dilakukan pencarian urutan terakhir (mirip seperti pencarian nama folder) dan dilanjutkan pada fungsi mkzip.
- [ ] Folder yang akan dizip akan disimpan pada folds.
- [ ] Dilakukan zip pada folder tersebut dan setelah dilakukan zip maka folder dapat diremove.

#### Kendala dan Catatan Soal 2
Kendala pada soal ini adalah mencari cara bagaimana mendapatkan gambar hasil pencarian "indonesia" dimana setiap gambar dapat dibedakan. Kami mendapatkan perintah untuk mendownload yakni wget. Namun perintah ini hanya dapat mendownload langsung dari url gambar tersebut, bukan dari hasil pencarian google. Akhirnya, kami menemukan caranya dengan mencari html dari halaman tersebut dan mengambil link url img src yang ada. 

### Soal 3 
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
    <br>
    - "${#password}" -ge 8 <br> 
        Minimal 8 karakter
    - "$password" == *[a-z]* && "$password" == *[A-Z]*<br>
        Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    -  "$password" == *[0-9]*<br> 
        Alphanumeric
    - "$password" != "$username"<br>
        Tidak boleh sama dengan username 
    - "$password" != *"chicken"* && "$password" != *"ernie"*<br>
        Tidak boleh menggunakan kata chicken atau ernie

    <br>    

<br>
command line untuk logic pada sub soal 3

``` sh
#!/bin/bash

if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[a-z]* && "$password" == *[A-Z]* && "$password" == *[0-9]* && "$password" != *"chicken"* && "$password" != *"ernie"* ]]
```
<br>

<br>
command lengkap dalam louis.sh sebagai script register

``` sh
#!/bin/bash

backup_time=$(date +"%Y:%m:%d %H:%M:%S")

read -p "Username : " username
if grep -q ^$username /home/lihardo04/Desktop/soal3/users/users.txt; 
then
 echo $"$backup_time REGISTER: ERROR user already exist"
 echo $"$backup_time REGISTER: ERROR user already exist" >> log.txt
 exit 1
fi

read -p "Password : " password

if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[a-z]* && "$password" == *[A-Z]* && "$password" == *[0-9]* && "$password" != *"chicken"* && "$password" != *"ernie"* ]]
then
	echo $"$username,$password" >> /home/lihardo04/Desktop/soal3/users/users.txt 
	echo $"$backup_time REGISTER: INFO user $username registered succesfully"
	echo $"$backup_time REGISTER: INFO user $username registered succesfully" >> /home/lihardo04/Desktop/soal3/log.txt

fi
```
<br>

- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

    - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists

        <br>

        ``` sh
        #!/bin/bash

        if grep -q ^$username /home/lihardo04/Desktop/soal3/users/users.txt; 
        then
        echo $"$backup_time REGISTER: ERROR user already exist"
        ```
        <br>


    - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully


        <br>

        ``` sh
        #!/bin/bash

        if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[a-z]* && "$password" == *[A-Z]* && "$password" == *[0-9]* && "$password" != *"chicken"* && "$password" != *"ernie"* ]]
        then
            echo $"$username,$password" >> /home/lihardo04/Desktop/soal3/users/users.txt 
            echo $"$backup_time REGISTER: INFO user $username registered succesfully"
            echo $"$backup_time REGISTER: INFO user $username registered succesfully" >> /home/lihardo04/Desktop/soal3/log.txt
        ```
        <br>
             
    - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
        <br>

        ``` sh
        #!/bin/bash

        check=$(awk -F, -v username="${username}" -v password="${password}" '$1==username && $2==password {print 1}' /home/lihardo04/Desktop/soal3/users/users.txt)

            if [[ "$check" == "1" ]] 
            then
            echo $"$backup_time LOGIN: INFO user $username logged in"
            echo $"$backup_time LOGIN: INFO user $username logged in" >> /home/lihardo04/Desktop/soal3/log.txt 
            else
            echo $"$backup_time LOGIN: ERROR failed login attempt on $username"
            echo $"$backup_time LOGIN: ERROR failed login attempt on $username" >> /home/lihardo04/Desktop/soal3/log.txt
            fi 
        fi
        ```
        kondisi ketiga terdapat pada logic setelah else
        ___
        <br>

    - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in


        <br>

        ``` sh
        #!/bin/bash

        check=$(awk -F, -v username="${username}" -v password="${password}" '$1==username && $2==password {print 1}' /home/lihardo04/Desktop/soal3/users/users.txt)

            if [[ "$check" == "1" ]] 
            then
            echo $"$backup_time LOGIN: INFO user $username logged in"
            echo $"$backup_time LOGIN: INFO user $username logged in" >> /home/lihardo04/Desktop/soal3/log.txt 
            else
            echo $"$backup_time LOGIN: ERROR failed login attempt on $username"
            echo $"$backup_time LOGIN: ERROR failed login attempt on $username" >> /home/lihardo04/Desktop/soal3/log.txt
            fi 
        fi
        ```
        kondisi keempat terdapat setelah if condition dan setelah then
        ___
        <br>

> Output Password tidak sesuai kriteria
![Screenshot_2023-03-10_114908](/uploads/409879fe4f6cd0a65cd6359741975957/Screenshot_2023-03-10_114908.png)

<br>

> Output Password sesuai kriteria
![Screenshot_2023-03-10_115150](/uploads/8e5971ca4eb27042418b52574f79cf7f/Screenshot_2023-03-10_115150.png)

<br>

<br>

> Proses registrasi ketika diketahui memasukkan username yang sudah ada
![Screenshot_2023-03-10_120217](/uploads/934f25d311da55ab284f5be01d5ec75a/Screenshot_2023-03-10_120217.png)

<br>

> Proses registrasi ketika Berhasil registrasi
![Screenshot_2023-03-10_120516](/uploads/40469b198c3ac03bca131fe5deb7260c/Screenshot_2023-03-10_120516.png)

<br>

<br>
> Proses login ketika memasukkan password yang salah
![Screenshot_2023-03-10_120906](/uploads/3c2613b9ab1bcb0c3750c14f76bfa1d0/Screenshot_2023-03-10_120906.png)

Username asli **Dominic** dengan password **ABcdefgh89**
___

<br>

<br>

> Proses login ketika Berhasil 
![Screenshot_2023-03-10_121134](/uploads/d669de4a037ae395d880321047ed949f/Screenshot_2023-03-10_121134.png)

<br>

#### Kendala dan Catatan Soal 3
perintah dari soal cukup jelas sehingga tidak ada kendala yang besar terjadi, tetapi hanya kendala teknis dimana kita harus mencari tahu operator - operator yang berperan sebagai pengatur kondisi (*conditional state*).

### Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:

    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a

- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

```sh
enkripsi(){
	Z_ascii=90 
	hour=$(date +"%H")
	if [ ${hour:0:1} == 0 ]; then
		hour=${hour:1:1}
	fi
	z_ascii=122
	alph=26
	while read -r line; do
		for ((i=0;i<${#line};i++)); do
			char=${line:$i:1}
			if [[ "$char" =~ [a-z] ]]; then
				ascii=$(printf "%d" "'$char'")
				new_ascii=$((ascii + hour))
				if [ $new_ascii -gt $z_ascii ]; then
					new_ascii=$((new_ascii - alph))
				fi
				new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
				echo -n "$new_char"
			elif [[ "$char" =~ [A-Z] ]]; then
				ascii=$(printf "%d" "'$char'")
                                new_ascii=$((ascii + hour))
                                if [ $new_ascii -gt $Z_ascii ]; then
                                        new_ascii=$((new_ascii - alph))
                                fi
                                new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
				echo -n "$new_char"
			else 
				echo -n "$char"
			fi	
		done
		echo ""
	done
}
name="$(date +'%H:%M %d:%m:%Y').txt"
cat /var/log/syslog | enkripsi > "$name"

#cron
# 0 */2 * * * log_encrypt.sh
```
- [ ] Untuk melakukan enkripsi maka pertama akan dilakukan pengambilan variabel hour dengan cara yang sama seperti pada soal 2. Variabel lain yang akan digunakan juga dimasukkan.

- [ ] Perintah while akan melakukan proses pada setiap line yang didapatkan. For akan mengambil setiap char dan melakukan proses pada char tersebut. 

- [ ] Proses yang dilakukan adalah mengecek apakah char tersebut merupakan a-z atau A-Z. Jika iya maka akan dilakukan penambahan nilai ascii. Jika penambahan tersebut membuat char menjadi keluar dari a-z atau A-Z maka akan dilakukan pengurangan dengan jumlah alphabet (26) sehingga char akan kembali kedalam a-z atau A-Z.

- [ ] Variabel name akan menyimpan nama file yang akan dibuat menggunakan  date. Setelah itu maka syslog dapat diambil, dilakukan enkripsi, serta dimasukkan dalam nama file.

```sh
#!/bin/bash
echo "File untuk di dekripsi?"
read filename
echo "Jam file dibuat?"
read hour
dekripsi(){
	A_ascii=65
	a_ascii=97
	alph=26
	while read -r line; do
		for ((i=0;i<${#line};i++)); do
			char=${line:$i:1}
                        if [[ "$char" =~ [a-z] ]]; then
                                ascii=$(printf "%d" "'$char'")
                                new_ascii=$((ascii - hour))
                                if [ $new_ascii -lt $a_ascii ]; then
                                        new_ascii=$((new_ascii + alph))
                                fi
				new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
                                echo -n "$new_char"
                        elif [[ "$char" =~ [A-Z] ]]; then
                                ascii=$(printf "%d" "'$char'")
                                new_ascii=$((ascii - hour))
                                if [ $new_ascii -lt $A_ascii ]; then
                                        new_ascii=$((new_ascii + alph))
                                fi
				new_char=$(printf "\\$(printf '%03o' "$new_ascii")")
                                echo -n "$new_char"
                        else 
                                echo -n "$char"
                        fi      
                done
                echo ""
        done
}

cat "$filename" | dekripsi 
```
- [ ] Proses dekripsi merupakan kebalikan dari proses enkripsi.
- [ ] Pertama akan diinputkan file apa yang perlu didekripsi dan jam file tersebut dibuat. Setelah itu akan dilakukan proses kebalikan dari enkripsi. Yang sebelumnya pertambahan ascii akan menjadi pengurangan, begitu juga sebaliknya. 
- [ ] Perintah terakhir adalah mengambil isi file dan melakukan proses dekripsi. Hasil akan langsung terlihat di terminal.

#### Kendala dan Catatan Soal 4
Kendala terdapat pada saat mencari tahu bagaimana operasi ascii pada bash. Pada bahasa pemrograman lain yang kami ketahui, operasi ascii cukup simpel yakni dengan menambahkan integer pada char. Akan tetapi pada bash ini perlu dilakukan konversi char ke ascii, dilakukan operasi, dan kembali lagi ke char.

Kami juga mendapatkan masukan untuk pengambilan nilai jam pada dekripsi. Nilai jam dapat langsung diambil dari nama file sehingga tidak diperlukan input lagi.
