#!/bin/bash

backup_time=$(date +"%Y:%m:%d %H:%M:%S")

read -p "Username : " username
if grep -q ^$username /home/lihardo04/Desktop/soal3/users/users.txt; 
then
 echo $"$backup_time REGISTER: ERROR user already exist" >> log.txt
 exit 1
fi

read -p "Password : " password

if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[a-z]* && "$password" == *[A-Z]* && "$password" == *[0-9]* && "$password" != *"chicken"* && "$password" != *"ernie"* ]]
then
	echo $"$username,$password" >> /home/lihardo04/Desktop/soal3/users/users.txt
	echo $"$backup_time REGISTER: INFO user $username registered succesfully" >> /home/lihardo04/Desktop/soal3/log.txt
fi
