#!/bin/bash

echo -e  "SOAL-1\n"
sort -t"," -n -k1 '2023 QS World University Rankings.csv' | awk -F "," '/Japan/ {print $1"    " $2"    "$3}' | head -n 5 
echo -e  "\n"
echo -e  "SOAL-2\n"
sort -t, -g -k9 '2023 QS World University Rankings.csv' | awk -F "," '/Japan/ {print $1"    " $2"    "$3"    FSR-Score ->"$9}' | head -5
echo -e "\n"
echo -e  "SOAL-3\n"
sort -t, -n -k20 '2023 QS World University Rankings.csv' | awk -F "," '/Japan/ {print $1"    " $2"    "$3"    Ger-Rank->"$20}' | head -10 
echo -e "\n"
echo -e  "SOAL-4\n"
grep -i "keren" '2023 QS World University Rankings.csv'| awk -F "," '{print $1"    "$2"    "$3}'
echo -e "\n"
