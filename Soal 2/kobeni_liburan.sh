#! /bin/bash

hour=$(date +"%H")
if [ ${hour:0:1} == 0 ];then
	hour=${hour:1:1}
fi
if [ $hour == 0 ]; then
	hour=1
fi

mkfolder(){
	read urutan
	if [ -n $urutan ];then
		urutan=$(($urutan + 1))
		mkdir "kumpulan_$urutan"
		echo "kumpulan_$urutan"
	else 
		mkdir "kumpulan_1"
		echo "kumpulan_1"
	fi
}

download(){
        read folder
		cd "$folder"
        for ((count=2;count<=hour+1;count++)); do
				link=$(wget --user-agent 'Mozilla/5.0' -qO - "www.google.be/search?q=indonesia\&tbm=isch" | sed 's/</\n</g' | grep '<img' | head -n"$count" | tail -n1 | sed 's/.*src="\([^"]*\)".*/\1/')
                wget -O "perjalanan_$(($count - 2)).jpg" $link
        done
}
#download masuk folder
# 0 */10 * * *
ls | grep 'kumpulan_' | awk -F '_' '{print $2}' | awk -F '.' '{print $1}' | tail -n 1 | mkfolder | download


mkzip(){
        read urutan
        if [ -n $urutan ];then
                urutan=$(($urutan + 1))
                echo "devil_$urutan.zip"
        else 
                echo "devil_1.zip"
        fi 
}


zipname=$(ls | grep 'devil_' | awk -F '_' '{print $2}' | awk -F '.' '{print $1}' | tail -n 1 | mkzip)

folds=$(ls | grep 'kumpulan_' | sort | tail -n 3) 

#melakukan zip
# 0 0 * * *
zip -r "$zipname" $folds

rm -r $folds
